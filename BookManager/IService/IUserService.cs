﻿using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookTool.Service
{
    interface IUserService
    {
        User GetUser(User user, out string databaseError);

        bool InsertUser(User user, out string databaseError);
    }
}
