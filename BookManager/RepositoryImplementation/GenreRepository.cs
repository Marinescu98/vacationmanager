﻿using BookManager.IRepository;
using BookTool.Models;
using BookTool.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BookTool.Repository
{
    public class GenreRepository : IGenreRepository
    {
        private SqlConnection connection;
        public GenreRepository()
        {
            connection = new SqlConnection(Constants.CONNECTION_STRING);
        }
        public List<Genre> SelectGenre(out string databaseError)
        {
            databaseError = null;

            List<Genre> genres = new List<Genre>();

            try
            {
                string sql = "SELECT * FROM T_GENRE";

                connection.Open();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                genres.Add(
                                 new Genre
                                 {
                                     ID = Int32.Parse(reader["ID"].ToString()),
                                     Name = reader["NAME"].ToString()
                                 });
                            }
                            return genres;
                        }
                        return null;
                    }
                }

            }
            catch (Exception exception)
            {
                databaseError = exception.ToString();
                return null;
            }
        }
    }
}