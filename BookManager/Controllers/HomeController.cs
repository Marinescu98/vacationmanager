﻿using BookManager.IService;
using BookManager.ServiceImplementation;
using BookTool.Models;
using BookTool.Service;
using BookTool.ServiceImpl;
using BookTool.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookManager.Controllers
{
    public class HomeController : Controller
    {
        
        private IBookService bookService = new BookServiceImpl();
        private IAuthorService authorService = new AuthorServiceImpl();
        private IGenreService genreService = new GenreServiceImpl();
        private IToReadService toReadService = new ToReadServiceImpl();
        private IUserService userService = new UserServiceImpl();

        #region web services
        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("Home");
        }
        [HttpGet]
        public ActionResult Home()
        {
            if (Session["Email"] as string == null)
                return RedirectToAction("Account/Index");

            return View();
        }
        #endregion

        #region populate DataTable
        public JsonResult DataTablePopulateBook()
        {
            string databaseError = null;

            if (String.IsNullOrEmpty(Session["Email"] as string))
                return new JsonResult { Data = new EmptyResult(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };

            List<ToRead> books = toReadService.SelectDataToRead(Session["Email"] as string, out databaseError);

            if (books == null)
            {
                if (!String.IsNullOrEmpty(databaseError))
                    return Json(Constants.Error.DATABASE_ERROR_CODE + "," + databaseError, JsonRequestBehavior.AllowGet);

                return new JsonResult { Data = new EmptyResult(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = books, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        #endregion

        #region define insert method
        public JsonResult InsertBook(Book model)
        {
            string databaseError = null;

            if (model == null)
                return Json(Constants.Error.BAD_REQUEST_CODE + "," + Constants.Error.BAD_REQUEST_MESSAGE, JsonRequestBehavior.AllowGet);

            if (!String.IsNullOrEmpty(databaseError) || bookService.InsertBook(model, out databaseError) == false)
            {
                if (!String.IsNullOrEmpty(databaseError))
                    return Json(Constants.Error.DATABASE_ERROR_CODE + "," + databaseError, JsonRequestBehavior.AllowGet);
            }

            return Json(Constants.Succes.SUCCESS_CODE + "," + Constants.Succes.SUCCESS_MESSAGE, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InsertToRead(ToRead model)
        {
            string databaseError = null;

            if (model == null)
                return Json(Constants.Error.BAD_REQUEST_CODE + "," + Constants.Error.BAD_REQUEST_MESSAGE, JsonRequestBehavior.AllowGet);

            if (String.IsNullOrEmpty(Session["Email"] as string))
                return Json(Constants.Error.REDIRECT_CODE + "," + Constants.Error.REDIRECT_MESSAGE, JsonRequestBehavior.AllowGet);

            model.UserBook = userService.GetUser(new User { Email = Session["Email"] as string }, out databaseError);

            if (toReadService.InsertBookT_TOREAD(model, out databaseError) == false)
            {
                if (!String.IsNullOrEmpty(databaseError))
                    return Json(Constants.Error.DATABASE_ERROR_CODE + "," + databaseError, JsonRequestBehavior.AllowGet);
                else
                    return Json(Constants.Error.REDIRECT_CODE + "," + Constants.Error.REDIRECT_MESSAGE, JsonRequestBehavior.AllowGet);
            }

            return Json(Constants.Succes.SUCCESS_CODE + "," + Constants.Succes.SUCCESS_MESSAGE, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InsertAuthor(string author)
        {
            string databaseError = null;

            if (String.IsNullOrEmpty(author))
                return Json(Constants.Error.BAD_REQUEST_CODE + "," + Constants.Error.BAD_REQUEST_MESSAGE, JsonRequestBehavior.AllowGet);

            if (Constants.DataBaseOperation.IsAlreadyInsert("SELECT * FROM T_AUTHOR WHERE NAME='" + author + "'", out databaseError) == true)
                return Json(Constants.Error.INFORMATION_CODE + "," + Constants.Error.INFORMATION_MESSAGE, JsonRequestBehavior.AllowGet);

            if (!String.IsNullOrEmpty(databaseError))
                return Json(Constants.Error.DATABASE_ERROR_CODE + "," + databaseError, JsonRequestBehavior.AllowGet);

            if (Constants.DataBaseOperation.InsertOneField(author, "NAME", "INSERT INTO T_AUTHOR(NAME) VALUES('" + author + "')", out databaseError) == false)
            {
                if (!String.IsNullOrEmpty(databaseError))
                    return Json(Constants.Error.DATABASE_ERROR_CODE + "," + databaseError, JsonRequestBehavior.AllowGet);
            }

            return Json(Constants.Succes.SUCCESS_CODE + "," + Constants.Succes.SUCCESS_MESSAGE, JsonRequestBehavior.AllowGet);
        }


        public JsonResult InsertGenre(string genre)
        {
            string databaseError = null;

            if (String.IsNullOrEmpty(genre))
                return Json(Constants.Error.BAD_REQUEST_CODE + "," + Constants.Error.BAD_REQUEST_MESSAGE, JsonRequestBehavior.AllowGet);

            if (Constants.DataBaseOperation.IsAlreadyInsert("SELECT * FROM T_GENRE WHERE NAME='" + genre + "'", out databaseError) == true)
                return Json(Constants.Error.INFORMATION_CODE + "," + Constants.Error.INFORMATION_MESSAGE, JsonRequestBehavior.AllowGet);

            if (!String.IsNullOrEmpty(databaseError))
                return Json(Constants.Error.DATABASE_ERROR_CODE + "," + databaseError, JsonRequestBehavior.AllowGet);

            if (Constants.DataBaseOperation.InsertOneField(genre, "NAME", "INSERT INTO T_GENRE(NAME) VALUES('" + genre + "')", out databaseError) == false)
            {
                if (!String.IsNullOrEmpty(databaseError))
                    return Json(Constants.Error.DATABASE_ERROR_CODE + "," + databaseError, JsonRequestBehavior.AllowGet);
            }

            return Json(Constants.Succes.SUCCESS_CODE + "," + Constants.Succes.SUCCESS_MESSAGE, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region dropDownList

        public JsonResult DropDownListAuthor()
        {
            string databaseError = null;

            List<Author> authors = authorService.SelectAuthors(out databaseError);

            if (authors == null)
            {
                if (!String.IsNullOrEmpty(databaseError))
                    return Json(Constants.Error.DATABASE_ERROR_CODE + "," + databaseError, JsonRequestBehavior.AllowGet);

                return new JsonResult { Data = new EmptyResult(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = authors, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult DropDownListGenre()
        {
            string databaseError = null;

            List<Genre> genres = genreService.SelectGenre(out databaseError);

            if (genres == null)
            {
                if (!String.IsNullOrEmpty(databaseError))
                    return Json(Constants.Error.DATABASE_ERROR_CODE + "," + databaseError, JsonRequestBehavior.AllowGet);

                return new JsonResult { Data = new EmptyResult(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = genres, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult DropDownListBook()
        {
            string databaseError = null;

            List<Book> books = bookService.SelectBook(out databaseError);

            if (books == null)
            {
                if (!String.IsNullOrEmpty(databaseError))
                    return Json(Constants.Error.DATABASE_ERROR_CODE + "," + databaseError, JsonRequestBehavior.AllowGet);

                return new JsonResult { Data = new EmptyResult(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = books, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        #endregion

        #region define get methods
        public JsonResult GetBookById(string id)
        {
            if (String.IsNullOrEmpty(id) || id.Equals("undefined") == true)
                return new JsonResult { Data = new EmptyResult(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };

            if (String.IsNullOrEmpty(Session["Email"] as string))
                return Json(Constants.Error.REDIRECT_CODE, JsonRequestBehavior.AllowGet);

            string databaseError = null;

            ToRead book = toReadService.GetBook(Int32.Parse(id),  out databaseError);


            if (book == null)
            {
                return new JsonResult { Data = new EmptyResult(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return new JsonResult { Data = book, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        #endregion

        #region update methods
        public JsonResult UpdateBookToRead(ToRead read)
        {
            if (read == null)
            {
                return Json(Constants.Error.BAD_REQUEST_CODE + "," + Constants.Error.BAD_REQUEST_MESSAGE, JsonRequestBehavior.AllowGet);
            }

            string errorMessage = String.Empty;

            if (bookService.UpdateBook(read.BookAbout, out errorMessage) == false)
            {
                if (!String.IsNullOrEmpty(errorMessage))
                    return Json(Constants.Error.DATABASE_ERROR_CODE + "," + errorMessage, JsonRequestBehavior.AllowGet);
            }

            if (toReadService.UpdateToRead(read, out errorMessage) == false)
            {
                if (!String.IsNullOrEmpty(errorMessage))
                    return Json(Constants.Error.DATABASE_ERROR_CODE + "," + errorMessage, JsonRequestBehavior.AllowGet);
            }

            return Json(Constants.Succes.SUCCESS_CODE + "," + Constants.Succes.SUCCESS_MESSAGE, JsonRequestBehavior.AllowGet);
        }
        #endregion

      
    }
}