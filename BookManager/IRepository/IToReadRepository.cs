﻿using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManager.IRepository
{
    public interface IToReadRepository:IBaseRepository<ToRead>
    {
        bool InsertBookT_TOREAD(ToRead book, out string databaseError);
        List<ToRead> Select(string email, out string databaseError);
        ToRead GetBook(int id, out string databaseError);
    }
}
