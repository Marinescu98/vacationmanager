﻿using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManager.IRepository
{
    public interface IAuthorRepository
    {
        List<Author> SelectAuthors(out string databaseError);
    }
}
