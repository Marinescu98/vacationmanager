﻿using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManager.IRepository
{
    public interface IBookRepository:IBaseRepository<Book>
    {
        bool InsertBook(Book book, out string databaseError);
        List<Book> SelectBook(out string databaseError);
    }
}
