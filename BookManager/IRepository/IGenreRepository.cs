﻿using BookTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManager.IRepository
{
    public interface IGenreRepository
    {
        List<Genre> SelectGenre(out string databaseError);
    }
}
